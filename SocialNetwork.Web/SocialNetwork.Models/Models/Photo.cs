﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SocialNetwork.Models
{
    public class Photo
    {
        [Key]
        public int Id { get; set; }
        public byte[] PhotoAsBytes { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
        public int PostsByLoggedUserInTimeline { get; set; }
        public string Type { get; set; }
        public int PhotoAsNumber { get; set; }
        public bool IsDeleted { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}