﻿using Microsoft.AspNetCore.Identity;
using SocialNetwork.Models.Enums;
using SocialNetwork.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SocialNetwork.Models
{
    public class User : IdentityUser<int>
    {
        public User()
        {
            Messages = new HashSet<Message>();
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public override string Email { get; set; }
        public override bool EmailConfirmed { get; set; } = true;
        public override string UserName { get; set; }
        public override string NormalizedEmail { get; set; }
        public override string NormalizedUserName { get; set; }
        public override bool LockoutEnabled { get; set; } = false;
        public override string PasswordHash { get; set; }
        //DO NOT TOUCH: CountryId, Country!
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        public string DateOfBirth { get; set; }
        public string ProfilePhoto { get; set; }
        public string CoverPhoto { get; set; }
        public string MonthOfBirth { get; set; }
        public string YearOfBirth { get; set; }
        public string Gender { get; set; }
        public string LoggedUserProfilePicture { get; set; }
        public int CurrentLoggedUser { get; set; }
        public string Education { get; set; }
        public bool IsDeleted { get; set; }
        public string JobTitle { get; set; }
        public string City { get; set; }
        public string Biography { get; set; }
        public int CurrentLoggedUserId { get; set; }
        public bool IsAdmin { get; set; }
        public string NewCommentContent { get; set; }
        public Visibility Visibility { get; set; } 
        public ICollection<Photo> Photos { get; set; }
        public ICollection<HistoryLog> HistoryLog { get; set; } = new List<HistoryLog>();
        public RelationshipStatus RelationshipStatus { get; set; }
        public virtual ICollection<UserFriend> RequestersUserFriends { get; set; }
        public virtual ICollection<UserFriend> AdresseesUserFriends { get; set; }
        [NotMapped]
        public ICollection<int> Friends1 { get; set; }
        [NotMapped]
        public ICollection<int> Friends2 { get; set; }
        public ICollection<Post> NewsFeed { get; set; }
        public virtual ICollection<User> Friends { get; set; }
        public ICollection<Post> Posts { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public ICollection<Comment> Comments { get; set; }

    }
}
