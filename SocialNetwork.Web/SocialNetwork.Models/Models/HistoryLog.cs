﻿using System.ComponentModel.DataAnnotations;

namespace SocialNetwork.Models
{
    public class HistoryLog
    {
        [Key]
        public int Id { get; set; }
        public string Content { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
