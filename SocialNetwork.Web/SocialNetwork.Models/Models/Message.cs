﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SocialNetwork.Models
{
    public class Message
    {
        public int Id { get; set; }
        public int? SenderId { get; set; }
        public virtual User Sender { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public DateTime SendOn { get; set; } = DateTime.Now;
        public int ReceiverId { get; set; }
        public virtual User Receiver { get; set; }
        public bool isSeen { get; set; }
        public bool IsDeleted { get; set; }
    }
}