﻿namespace SocialNetwork.Models
{
    public enum RelationshipStatus
    {
        NotSet = 0,
        Single = 1,
        InRelationship = 2,
        Engaged = 3,
        Married = 4,
        Divorced = 5
    }
}