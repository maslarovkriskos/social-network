﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Models.Enums
{
    public enum Visibility
    {
        Public = 0,
        OnlyForFriends = 1
    }
}
