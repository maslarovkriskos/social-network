﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialNetwork.Database;
using SocialNetwork.Models;
using SocialNetwork.Services;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.Services;
using System;
using System.Threading.Tasks;

namespace SocialNetwork.Test
{
    [TestClass]
    public class CommentShould
    {
        [TestMethod]
        public async Task Create_Comment_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Create_Comment_Should));

            var comment = new Comment
            {
                Id = 1,
                UserId = 1,
                PostId = 1
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Comments.Add(comment);
                await arrangeContext.SaveChangesAsync();
                Assert.AreEqual(comment.Id, comment.Id);
            }
        }
        [TestMethod]
        public async Task Delete_Comment_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Delete_Comment_Should));

            var comment = new Comment
            {
                Id = 1,
                UserId = 1,
                PostId = 1
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Comments.Add(comment);
                await arrangeContext.SaveChangesAsync();
                arrangeContext.Comments.Remove(comment);
                Assert.IsTrue(comment.IsDeleted = true);
            }
        }
    }
}
