﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialNetwork.Database;
using SocialNetwork.Models;
using SocialNetwork.Services;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Mappers;
using System.Threading.Tasks;

namespace SocialNetwork.Test
{
    [TestClass]
    public class UserShould
    {
        [TestMethod]
        public async Task Create_User_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Create_User_Should));

            var user = new User
            {
                Id = 1,
                FirstName = "Nik",
                LastName = "Nikolov",
                Email = "nnickolov1@gmail.com",
                EmailConfirmed = true
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(user);
                await arrangeContext.SaveChangesAsync();
                var sut = new UserService(arrangeContext);
                var result = await sut.GetUserAsync(1);
                Assert.AreEqual(user.Id, result.Id);
            }
        }
        [TestMethod]
        public async Task Delete_User_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Delete_User_Should));

            var user = new User
            {
                Id = 1,
                FirstName = "Nik",
                LastName = "Nikolov",
                Email = "nnickolov1@gmail.com",
                EmailConfirmed = true
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(user);
                await arrangeContext.SaveChangesAsync();
                arrangeContext.Users.Remove(user);
                var sut = new UserService(arrangeContext);
                var result = sut.GetUserAsync(1);
                Assert.AreNotEqual(user.Id, result.Id);
            }
        }
        [TestMethod]
        public async Task Update_User_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Update_User_Should));

            var user = new User
            {
                Id = 1,
                FirstName = "Nik",
                LastName = "Nikolov",
                Email = "nnickolov1@gmail.com",
                EmailConfirmed = true
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(user);
                await arrangeContext.SaveChangesAsync();
                var sut = new UserService(arrangeContext);

                var userDTO = new UserDTO();
                
                userDTO = user.GetDTO();

                userDTO.FirstName = "Kristiyan";

                var result = sut.UpdateExistingUserInformationAsync(1, userDTO);

                Assert.AreEqual(user.FirstName, userDTO.FirstName);
            }
        }
        [TestMethod]
        public async Task Get_User_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Get_User_Should));

            var user = new User
            {
                Id = 1,
                FirstName = "Nik",
                LastName = "Nikolov",
                Email = "nnickolov1@gmail.com",
                EmailConfirmed = true
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Users.Add(user);
                await arrangeContext.SaveChangesAsync();
                var sut = new UserService(arrangeContext);
                
                var result = await sut.GetUserAsync(1);

                
                Assert.AreEqual(user.Id, result.Id);
            }
        }
    }
}
