﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialNetwork.Database;
using SocialNetwork.Models;
using SocialNetwork.Services;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Mappers;
using SocialNetwork.Services.Services;
using System;
using System.Threading.Tasks;

namespace SocialNetwork.Test
{
    [TestClass]
    public class PostShould
    {
        [TestMethod]
        public async Task Delete_Post_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Delete_Post_Should));

            var post = new Post
            {
                Id = 1,
                CreatedOn = DateTime.Now,
                PhotoId = 1,
                UserId = 1
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Posts.Add(post);
                await arrangeContext.SaveChangesAsync();
                arrangeContext.Posts.Remove(post);
                var sut = new PostService(arrangeContext);
                var result = sut.GetAllPostsAsync();
                Assert.AreNotEqual(post.Id, result.Id);
            }
        }

        [TestMethod]
        public async Task Like_Post_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Like_Post_Should));

            var post = new Post
            {
                Id = 1,
                CreatedOn = DateTime.Now,
                PhotoId = 1,
                UserId = 1
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Posts.Add(post);
                await arrangeContext.SaveChangesAsync();
                var sut = new PostService(arrangeContext);

                var result = await sut.LikePostAsync(1);


                Assert.AreEqual(post.Likes, result.Likes);
            }
        }
        [TestMethod]
        public async Task Dislike_Post_Should()
        {
            // Arrange
            var options = Utils.GetOptions(nameof(Dislike_Post_Should));

            var post = new Post
            {
                Id = 1,
                CreatedOn = DateTime.Now,
                PhotoId = 1,
                UserId = 1
            };

            await using (var arrangeContext = new SocialNetworkDbContext(options))
            {
                arrangeContext.Posts.Add(post);
                await arrangeContext.SaveChangesAsync();
                var sut = new PostService(arrangeContext);

                var result = await sut.FlagPostAsync(1);


                Assert.AreEqual(post.Likes, result.Likes);
            }
        }
    }
}
