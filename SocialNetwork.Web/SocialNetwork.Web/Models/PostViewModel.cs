﻿using SocialNetwork.Models;

namespace SocialNetwork.Web.Models
{
    public class PostViewModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public Photo Photo { get; set; }
    }
}
