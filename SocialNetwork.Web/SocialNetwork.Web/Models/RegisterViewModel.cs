﻿using SocialNetwork.Models;

namespace SocialNetwork.Web.Models
{
    public class RegisterViewModel
    {    
        public string PasswordHash { get; set; }
        public string DateOfBirth { get; set; }
        public string MonthOfBirth { get; set; }
        public string YearOfBirth { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
    }
}
