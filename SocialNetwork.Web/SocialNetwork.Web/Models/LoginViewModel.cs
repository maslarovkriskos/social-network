﻿using System.ComponentModel.DataAnnotations;

namespace SocialNetwork.Web.Models
{

    public class LoginViewModel 
    {
            [Required]
            public string Email { get; set; }

            [Required]
            public string PasswordHash { get; set; }
    }
}
