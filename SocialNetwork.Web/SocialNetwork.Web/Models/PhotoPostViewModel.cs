﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Models
{
    public class PhotoPostViewModel
    {
        public IFormFile Photo { get; set; }
        public string PhotoDescription { get; set; }
    }
}
