﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SocialNetwork.Models;
using SocialNetwork.Models.Enums;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Models
{
    public class UpdateUserInformationViewModel
    {
        [Required(ErrorMessage = "Job title is required.")]
        [MinLength(5, ErrorMessage = "Job title should be least 5 symbols.")]
        public string JobTitle { get; set; }
        [Required(ErrorMessage = "Biography is required.")]
        [MinLength(5, ErrorMessage = "Biography should be least 5 symbols.")]
        public string Biography { get; set; }
        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }
        [Required(ErrorMessage = "Country is required.")]
        public int CountryId { get; set; }
        [Required(ErrorMessage = "Education is required.")]
        [MinLength(8, ErrorMessage = "Education should be least 8 symbols.")]
        public string Education { get; set; }
        [Required(ErrorMessage = "Photo description is required.")]
        public string PhotoDescription { get; set; }
        public Country Country { get; set; }
        [Required(ErrorMessage = "Visibility level is required.")]
        public Visibility Visibility { get; set; }
        [Required(ErrorMessage = "Profile photo is required.")]
        public IFormFile Photo { get; set; }
        [Required(ErrorMessage = "Cover photo is required.")]
        public IFormFile CoverPhoto { get; set; }
    }
}