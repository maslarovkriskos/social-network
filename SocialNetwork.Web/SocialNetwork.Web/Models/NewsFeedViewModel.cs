﻿using SocialNetwork.Models;
using System.Collections.Generic;

namespace SocialNetwork.Web.Models
{
    public class NewsFeedViewModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<Post> NewsFeed { get; set; }

    }
}
