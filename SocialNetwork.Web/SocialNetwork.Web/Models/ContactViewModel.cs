﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Models
{
    public class ContactViewModel
    {
        public string FirstAndLastName { get; set; }
        public string Email { get; set; }
        public string MessageContent { get; set; }
    }
}
