﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Web.Models;

namespace SocialNetwork.Web.Controllers
{
    public class AboutController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly UserManager<User> _userManager;
        public AboutController(IMapper mapper, UserManager<User> userManager, IUserService userService)
        {
            this._mapper = mapper;
            this._userManager = userManager;
            this._userService = userService;
        }
        // GET: AboutController
        public async Task<IActionResult> About(int id)
        {
            var user = await _userService.GetUserAsync(id);
            string curr = _userManager.GetUserId(User);

            user.CurrentLoggedUserId = int.Parse(curr);
            return View(user);
        }
    }
}
