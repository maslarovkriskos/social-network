﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;

namespace SocialNetwork.Web.Controllers
{
    public class AlbumController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly ICommentService _commentService;
        private readonly IContactService _contactService;
        private readonly IPhotoService _photoService;
        private readonly IPostService _postService;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;


        public AlbumController(IPostService postService, ICommentService commentService, ILogger<HomeController> logger, IMapper mapper, IContactService contactService, IPhotoService photoService, IUserService userService, SignInManager<User> signInManager, UserManager<User> userManager)
        {
            this._logger = logger;
            this._mapper = mapper;
            this._contactService = contactService;
            this._photoService = photoService;
            this._userService = userService;
            this._signInManager = signInManager;
            this._userManager = userManager;
            this._postService = postService;
            this._commentService = commentService;
        }
        // GET: Album
        public async Task<IActionResult> Album(int id)
        {
            var user = await _userService.GetUserAsync(id);

            string curr = _userManager.GetUserId(User);

            user.CurrentLoggedUserId = int.Parse(curr);

            return View(user);
        }
    }
}