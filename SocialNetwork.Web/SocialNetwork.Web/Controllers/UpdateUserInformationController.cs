﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Models;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Web.Models;

namespace SocialNetwork.Web.Controllers
{
    public class UpdateUserInformationController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IPhotoService _photoService;
        private readonly IWebHostEnvironment _environment;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public UpdateUserInformationController(IPhotoService photoService, IWebHostEnvironment environment, SignInManager<User> signInManager, IMapper mapper, IUserService userService, UserManager<User> userManager)
        {
            this._userService = userService;
            this._userManager = userManager;
            this._photoService = photoService;
            this._environment = environment;
            this._signInManager = signInManager;
            this._mapper = mapper;
        }
        //GET 
        public IActionResult UpdateExistingUserInformation()
        {
            return View();
        }

        // POST: UpdateUserInformationController
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateExistingUserInformation(int id, UpdateExistingUserInformatioViewModel updateExistingUserInformatio)
        {
            if (ModelState.IsValid)
            {
                var userDTO = _mapper.Map<UserDTO>(updateExistingUserInformatio);
                string curentUser = _userManager.GetUserId(User);

                if (updateExistingUserInformatio.Photo != null)
                {
                    var random = new Random();
                    var photoName = random.Next();

                    var extension = updateExistingUserInformatio.Photo.FileName.Split(".").Last();
                    var fileName = $"{photoName}.{extension}";
                    var destPath = Path.Combine(_environment.WebRootPath, "images", fileName);

                    using (var fileStream = new FileStream(destPath, FileMode.Create))
                    {
                        updateExistingUserInformatio.Photo.CopyTo(fileStream);
                    }

                    var memoryStream = new MemoryStream();
                    updateExistingUserInformatio.Photo.OpenReadStream().CopyTo(memoryStream);
                    byte[] photoAsBytes = memoryStream.ToArray();

                    var photoDTO = _mapper.Map<PhotoDTO>(updateExistingUserInformatio);
                    photoDTO.PhotoAsNumber = photoName;
                    photoDTO.Type = fileName;

                    photoDTO.PhotoAsBytes = photoAsBytes;
                    photoDTO.PhotoDescription = updateExistingUserInformatio.PhotoDescription;

                    userDTO.ProfilePhoto = fileName;
                    await _photoService.UploadPhotoAsync(0, int.Parse(_userManager.GetUserId(User)), photoDTO);
                }

                if (updateExistingUserInformatio.CoverPhoto != null)
                {
                    var randomCover = new Random();
                    var photoNameCover = randomCover.Next();

                    var extensionCover = updateExistingUserInformatio.CoverPhoto.FileName.Split(".").Last();
                    var fileNameCover = $"{photoNameCover}.{extensionCover}";
                    var destPathCover = Path.Combine(_environment.WebRootPath, "images", fileNameCover);

                    using (var fileStreamCover = new FileStream(destPathCover, FileMode.Create))
                    {
                        updateExistingUserInformatio.CoverPhoto.CopyTo(fileStreamCover);
                    }

                    var memoryStreamCover = new MemoryStream();
                    updateExistingUserInformatio.CoverPhoto.OpenReadStream().CopyTo(memoryStreamCover);
                    byte[] photoAsBytesCover = memoryStreamCover.ToArray();

                    var photoDTOCover = _mapper.Map<PhotoDTO>(updateExistingUserInformatio);
                    photoDTOCover.PhotoAsNumber = photoNameCover;
                    photoDTOCover.Type = fileNameCover;

                    photoDTOCover.PhotoAsBytes = photoAsBytesCover;

                    userDTO.CoverPhoto = fileNameCover;
                    await _photoService.UploadCoverPhotoAsync(int.Parse(_userManager.GetUserId(User)), photoDTOCover);
                }

                userDTO.Visibility = updateExistingUserInformatio.Visibility;

                await _userService.UpdateExistingUserInformationAsync(int.Parse(curentUser), userDTO);
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: UpdateUserInformationController/Delete/5
        public async Task<IActionResult> DeleteUser()
        {
            string curentUser = _userManager.GetUserId(User);

            var user = await _userService.GetUserAsync(int.Parse(curentUser));

            return View(user);
        }

        // POST: UpdateUserInformationController/Delete/5
        [HttpPost]
        public async Task<IActionResult> DeleteConfirmation()
        {
            string curentUser = _userManager.GetUserId(User);
            await _userService.DeleteUserAsync(int.Parse(curentUser));

            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
        public IActionResult DeleteRefuse()
        {
            return RedirectToAction("Index", "Home");
        }
    }
}
