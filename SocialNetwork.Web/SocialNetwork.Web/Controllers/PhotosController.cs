﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Web.Models;
using System.Threading.Tasks;

namespace SocialNetwork.Web.Controllers
{
    public class PhotosController : Controller
    {
        private readonly IPhotoService _photoService;
        private readonly IUserService _userService;
        private readonly IPostService _postService;
        private readonly IMapper _mapper;

        public PhotosController(IPhotoService photoService, IUserService userService, IPostService postService, IMapper mapper)
        {
            this._photoService = photoService;
            this._userService = userService;
            this._postService = postService;
            this._mapper = mapper;
        }

        // GET: Photos
        public IActionResult Index()
        {
            return View();
        }

        // GET: Photos/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var photo = await _context.Photos
        //        .Include(p => p.Post)
        //        .Include(p => p.User)
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (photo == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(photo);
        //}

        // GET: Photos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Photos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int id, UpdateUserInformationViewModel updateUserInformationViewModel)
        {
            var photoDTO = _mapper.Map<PhotoDTO>(updateUserInformationViewModel);

            if (ModelState.IsValid)
            {
                await _photoService.UploadPhotoAsync(0, id, photoDTO);
                return RedirectToAction(nameof(Index));
            }

            return View(photoDTO);
        }

        // GET: Photos/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var photo = await _context.Photos.FindAsync(id);
        //    if (photo == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["PostId"] = new SelectList(_context.Posts, "Id", "Id", photo.PostId);
        //    ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", photo.UserId);
        //    return View(photo);
        //}

        //// POST: Photos/Edit/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for 
        //// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,PhotoAsBytes,PostId,IsDeleted,UserId")] Photo photo)
        //{
        //    if (id != photo.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(photo);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!PhotoExists(photo.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["PostId"] = new SelectList(_context.Posts, "Id", "Id", photo.PostId);
        //    ViewData["UserId"] = new SelectList(_context.Users, "Id", "Id", photo.UserId);
        //    return View(photo);
        //}

        //// GET: Photos/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var photo = await _context.Photos
        //        .Include(p => p.Post)
        //        .Include(p => p.User)
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (photo == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(photo);
        //}

        //// POST: Photos/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //    {
        //        var photo = await _context.Photos.FindAsync(id);
        //        _context.Photos.Remove(photo);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }

        //    private bool PhotoExists(int id)
        //    {
        //        return _context.Photos.Any(e => e.Id == id);
        //    }
        //}
    }
}
