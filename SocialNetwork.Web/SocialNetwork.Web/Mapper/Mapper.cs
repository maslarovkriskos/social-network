﻿using AutoMapper;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Account.Internal;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Web.Models;

namespace SocialNetwork.Web.Mapper
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            CreateMap<CommentDTO, CommentViewModel>().ReverseMap();
            CreateMap<CountryDTO, CountryViewModel>().ReverseMap();
            CreateMap<PostDTO, PostViewModel>().ReverseMap();
            CreateMap<UserDTO, RegisterViewModel>().ReverseMap();
            CreateMap<UserDTO, LoginModel>().ReverseMap();
            CreateMap<UserDTO, ContactViewModel>().ReverseMap();
            CreateMap<MessageDTO, UserDTO>().ReverseMap();
            CreateMap<UserDTO, UpdateUserInformationViewModel>().ReverseMap();
            CreateMap<PhotoDTO, UpdateUserInformationViewModel>().ReverseMap();
            CreateMap<UserDTO, CommentDTO>().ReverseMap();
            CreateMap<UserDTO, UpdateExistingUserInformatioViewModel>().ReverseMap();
            CreateMap<PhotoDTO, UpdateExistingUserInformatioViewModel>().ReverseMap();
            CreateMap<UserDTO, PhotoPostViewModel>().ReverseMap();
            CreateMap<PhotoDTO, PhotoPostViewModel>().ReverseMap();
            CreateMap<PostDTO, UserDTO>().ReverseMap();
        }
    }
}