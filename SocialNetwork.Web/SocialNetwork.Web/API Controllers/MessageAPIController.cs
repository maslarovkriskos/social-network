﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Contracts;

namespace SocialNetwork.Web.API_Controllers
{
    [Route("api/message")]
    [ApiController]
    public class MessageAPIController : ControllerBase
    {
        private readonly IMessageService _service;
        public MessageAPIController(IMessageService service)
        {
            this._service = service;
        }
        //TODO
        [HttpPost("")]
        public async Task<IActionResult> CreateMessage([FromBody] MessageDTO messageDTO)
        {
            var message = await _service.CreateMessageAsync(messageDTO);
            return new JsonResult(message);
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMessage(int id)
        {
            var message = await _service.DeleteMessageAsync(id);
            return new JsonResult(message);
        }
        
        [HttpGet("")]
        public async Task<IActionResult> GetAllMessages()
        {
            var message = await _service.GetAllMessagesAsync();
            return Ok(message);
        }
        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMessage(int id)
        {
            try
            {
                var message = await _service.GetMessageAsync(id);
                return Ok(message);
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }
    }
}
