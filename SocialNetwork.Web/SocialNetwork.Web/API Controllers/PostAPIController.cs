﻿using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Mappers;
using System;
using System.Threading.Tasks;

namespace SocialNetwork.Web.API_Controllers
{
    [Route("api/post")]
    [ApiController]
    public class PostAPIController : ControllerBase
    {
        private readonly IPostService _service;
        public PostAPIController(IPostService service)
        {
            this._service = service;
        }
        //APIOK
        [HttpPost("")]
        public async Task<IActionResult> CreatePost(int loggedUser, int id, [FromBody] PostDTO postDTO)
        {
            var post = await _service.CreatePostAsync(loggedUser, id, postDTO);
            return new JsonResult(post);
        }
        //APIOK
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id, int userId)
        {
            var post = await _service.DeletePostAsync(id, userId);
            return new JsonResult(post);
        }
        //APIOK
        [HttpGet("")]
        public async Task<IActionResult> GetAllPosts()
        {
            var post = await _service.GetAllPostsAsync();
            return Ok(post);
        }
        //APIOK
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPost(int id)
        {
            try
            {
                var post = await _service.GetPostAsync(id);
                return Ok(post);
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }
        //APIOK
        [HttpPut("")]
        public async Task<IActionResult> UpdatePost([FromQuery] int id, [FromBody] PostDTO postDTO)
        {
            var post = await _service.UpdatePostAsync(id, postDTO);
            return new JsonResult(post);
        }
        //APIOK
        [HttpPut("like/id={id}")]
        public async Task<IActionResult> LikePost(int id)
        {
            var post = await _service.LikePostAsync(id);
            return new JsonResult(post);
        }
        //APIOK
        [HttpPut("flag/id={id}")]
        public async Task<IActionResult> FlagPost(int id)
        {
            var post = await _service.FlagPostAsync(id);
            return new JsonResult(post);
        }
    }
}
