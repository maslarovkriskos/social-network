﻿using SocialNetwork.Services.DTOs;
using System.Threading.Tasks;

namespace SocialNetwork.Services.Contracts
{
    public interface IContactService
    {
        Task<MessageDTO> SendMessageToAdmin(UserDTO userDTO);
    }
}
