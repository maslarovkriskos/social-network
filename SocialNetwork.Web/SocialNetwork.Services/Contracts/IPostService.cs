﻿using System.Threading.Tasks;
using System.Collections.Generic;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Models.ResponseModels;

namespace SocialNetwork.Services.Contracts
{
    public interface IPostService
    {
        Task<PostDTO> UpdatePostAsync(int id, PostDTO postDTO);
        Task<ResponseModel> DeletePostAsync(int id, int userId);
        Task<PostDTO> CreatePostAsync(int loggedUser, int id, PostDTO postDTO);
        Task<PostDTO> GetPostAsync(int id);
        Task<ICollection<PostDTO>> GetAllPostsAsync();
        Task<PostDTO> LikePostAsync(int id);
        Task<PostDTO> FlagPostAsync(int id);
    }
}
