﻿using SocialNetwork.Models.ResponseModels;
using SocialNetwork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SocialNetwork.Services.Contracts
{
    public interface IMessageService
    {
        Task<MessageDTO> DeleteMessageAsync(int id);
        Task<ResponseModel> CreateMessageAsync(MessageDTO messageDTO);
        Task<MessageDTO> GetMessageAsync(int id);
        Task<ICollection<MessageDTO>> GetAllMessagesAsync();
    }
}
