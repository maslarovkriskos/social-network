﻿using SocialNetwork.Services.DTOs;
using System.Threading.Tasks;

namespace SocialNetwork.Services.Contracts
{
    public interface IPhotoService
    {
        Task<PhotoDTO> DeletePhotoAsync(int id);
        Task<PhotoDTO> UploadPhotoAsync(int loggedUser, int id, PhotoDTO photoDTO);
        Task<PhotoDTO> UploadCoverPhotoAsync(int id, PhotoDTO photoDTO);
    }
}
