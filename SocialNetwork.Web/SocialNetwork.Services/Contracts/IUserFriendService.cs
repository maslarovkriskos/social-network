﻿using System.Threading.Tasks;
using SocialNetwork.Models.ResponseModels;

namespace SocialNetwork.Services.Contracts
{
    public interface IUserFriendService
    {
        Task<ResponseModel> SendFriendRequest(int requesterId, int adresseeId);
        Task<ResponseModel> AcceptFriendRequest(int Id);
        Task<ResponseModel> DeclineFriendRequest(int Id);
        Task<ResponseModel> RemoveFriend(int Id);
    }
}
