﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Services.DTOs
{
    public class PhotoDTO
    {
        public int Id { get; set; }
        public byte[] PhotoAsBytes { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
        public int PostsByLoggedUserInTimeline { get; set; }
        public string Type { get; set; }
        public string PhotoDescription { get; set; }
        public int PhotoAsNumber { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
