﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Services.DTOs
{
    public class PostDTO
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsLiked { get; set; }
        public int Likes { get; set; }
        public bool IsFlagged { get; set; }
        public int LoggedUser { get; set; }
        public int Flags { get; set; }
        public int? PhotoId { get; set; }
        public Photo Photo { get; set; }
        public string Content { get; set; }
        public bool IsDeleted { get; set; }
        public string PostPhoto { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Photo> Photos { get; set; }
    }
}
