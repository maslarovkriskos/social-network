﻿using SocialNetwork.Models;
using System;

namespace SocialNetwork.Services.DTOs
{
    public class CommentDTO
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public int? PhotoId { get; set; }
        public Photo Photo { get; set; }
        public bool IsFlagged { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsLiked { get; set; }
        public int PostsByLoggedUserInTimeline { get; set; }
        public int Likes { get; set; }
        public int Flags { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime CreatedOn { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
    }
}
