﻿using SocialNetwork.Models;
using System.Collections.Generic;

namespace SocialNetwork.Services.DTOs
{
    public class CountryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<User> Users { get; set; } = new List<User>();
    }
}
