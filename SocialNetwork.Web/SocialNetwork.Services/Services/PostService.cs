﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Database;
using System.Linq;
using SocialNetwork.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using SocialNetwork.Models.ResponseModels;

namespace SocialNetwork.Services.Services
{
    public class PostService : IPostService
    {
        private readonly SocialNetworkDbContext _context;
        public PostService(SocialNetworkDbContext context)
        {
            this._context = context;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<PostDTO> CreatePostAsync(int loggedUser, int id, PostDTO postDTO)
        {
            var user = _context.Users.Where(x => x.Id == id).FirstOrDefault();

            var newPostDTO = new PostDTO
            {
                Content = postDTO.Content,
                PostPhoto = "",
                CreatedOn = DateTime.Now,
                UserId = postDTO.UserId,
                LoggedUser = loggedUser
            };

            _context.Posts.Add(newPostDTO.GetPost());
            await _context.SaveChangesAsync();

            var currPost = _context.Posts.Where(x => x.CreatedOn == newPostDTO.CreatedOn).FirstOrDefault();

            user.Posts.Add(currPost);

            var historyLog = new HistoryLogDTO
            {
                Content = $"{user.FirstName} added a new post at {currPost.CreatedOn}.",
                UserId = id
            };

            _context.HistoryLogs.Add(historyLog.GetHistoryLog());
            await _context.SaveChangesAsync();

            return newPostDTO;
        }

        //OK
        //APIOK
        //ControllerOK
        public async Task<ResponseModel> DeletePostAsync(int id, int userId)
        {
            var result = new ResponseModel();

            var post = await this._context.Posts
                .FirstOrDefaultAsync(x => x.Id == id);

            var photo = await this._context.Photos
            .FirstOrDefaultAsync(x => x.PostId == id);

            if (photo != null)
            {
                _context.Photos.Remove(photo);
                await _context.SaveChangesAsync();
            }

            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();

            var historyLog = new HistoryLogDTO
            {
                Content = $"Just deleted his post.",
                UserId = userId
            };

            _context.HistoryLogs.Add(historyLog.GetHistoryLog());

            await _context.SaveChangesAsync();

            result.Success = true;
            result.Message = "This post has been deleted successfully!";

            return result;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<ICollection<PostDTO>> GetAllPostsAsync()
        {
            var post = await (from p in _context.Posts
                              join u in _context.Users on p.Id equals u.Id into grp1
                              join c in _context.Comments on p.Id equals c.Id into grp2
                              from g in grp1.DefaultIfEmpty()
                              from cg in grp2.DefaultIfEmpty()
                              where p.IsDeleted == false
                              select new PostDTO
                              {
                                  Id = p.Id,
                                  LoggedUser = p.LoggedUser,
                                  UserId = p.UserId,
                                  User = p.User,
                                  Content = p.Content,
                                  PostPhoto = p.PostPhoto,
                                  Comments = p.Comments,
                                  CreatedOn = p.CreatedOn,
                                  Likes = p.Likes,
                                  Flags = p.Flags,
                              }).ToListAsync();


            return post;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<PostDTO> GetPostAsync(int id)
        {
            var post = await (from p in _context.Posts
                              join u in _context.Users on p.Id equals u.Id into grp1
                              join c in _context.Comments on p.Id equals c.Id into grp2
                              from g in grp1.DefaultIfEmpty()
                              from cg in grp2.DefaultIfEmpty()
                              where p.IsDeleted == false
                              select new PostDTO
                              {
                                  Id = p.Id,
                                  UserId = p.UserId,
                                  User = p.User,
                                  LoggedUser = p.LoggedUser,
                                  Content = p.Content,
                                  PostPhoto = p.PostPhoto,
                                  Comments = p.Comments,
                                  CreatedOn = p.CreatedOn,
                                  Likes = p.Likes,
                                  Flags = p.Flags,
                              }).FirstOrDefaultAsync();

            return post;
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<PostDTO> FlagPostAsync(int id)
        {
            var post = await this._context.Posts
                .FirstOrDefaultAsync(x => x.Id == id);

            post.IsFlagged = true;

            post.Flags++;

            await _context.SaveChangesAsync();

            return post.GetDTO();
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<PostDTO> LikePostAsync(int postId)
        {
            var post = await this._context.Posts
                .FirstOrDefaultAsync(x => x.Id == postId);

            post.IsLiked = true;

            post.Likes++;

            await _context.SaveChangesAsync();

            return post.GetDTO();
        }
        //OK
        //APIOK
        //ControllerOK
        public async Task<PostDTO> UpdatePostAsync(int id, PostDTO postDTO)
        {
            var post = await this._context.Posts
             .FirstOrDefaultAsync(x => x.Id == id);

            post.Content = postDTO.Content;

            post.Photo = postDTO.Photo;

            await _context.SaveChangesAsync();

            return post.GetDTO();
        }
    }
}
