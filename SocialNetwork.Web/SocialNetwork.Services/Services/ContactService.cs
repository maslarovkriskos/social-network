﻿using SocialNetwork.Database;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Services.DTOs;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.Services
{
    public class ContactService : IContactService
    {
        private readonly SocialNetworkDbContext _context;
        private readonly IMessageService _messageService;
        public ContactService(SocialNetworkDbContext context, IMessageService messageService)
        {
            this._context = context;
            this._messageService = messageService;
        }

        public async Task<MessageDTO> SendMessageToAdmin(UserDTO userDTO)
        {
            var user = _context.Users.Where(x => x.IsAdmin == true).FirstOrDefault();

            var message = new MessageDTO
            {
                SendOn = DateTime.Now,
                SenderId = 1,
                UserName = userDTO.FirstName + " / " + userDTO.Email,
                Text = userDTO.NewCommentContent,
                ReceiverId = 1
            };

            await _messageService.CreateMessageAsync(message);

            return message;
        }
    }
}