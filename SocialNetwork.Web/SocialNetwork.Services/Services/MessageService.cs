﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using SocialNetwork.Services.DTOs;
using SocialNetwork.Services.Contracts;
using SocialNetwork.Database;
using SocialNetwork.Models.ResponseModels;
using System.Linq;
using SocialNetwork.Services.Mappers;
using Microsoft.EntityFrameworkCore;

namespace SocialNetwork.Services.Services
{
    public class MessageService : IMessageService
    {
        private readonly SocialNetworkDbContext _context;
        public MessageService(SocialNetworkDbContext context)
        {
            this._context = context;
        }
        //OK
        public async Task<ResponseModel> CreateMessageAsync(MessageDTO messageDTO)
        {
            var result = new ResponseModel();

            _context.Messages.Add(messageDTO.GetMessage());

            await _context.SaveChangesAsync();

            result.Success = true;

            return result;
        }
        //OK
        public async Task<MessageDTO> DeleteMessageAsync(int id)
        {
            var message = await this._context.Messages
                .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            message.IsDeleted = true;

            await _context.SaveChangesAsync();

            return message.GetDTO();
        }
        //OK
        public async Task<ICollection<MessageDTO>> GetAllMessagesAsync()
        {
            var messages = await (from u in _context.Messages
                                  join c in _context.Users on u.SenderId equals c.Id into grp1
                                  from g in grp1.DefaultIfEmpty()
                                  where u.IsDeleted == false
                                  select new MessageDTO
                                  {
                                      Id = u.Id,
                                      SendOn = u.SendOn,
                                      Text = u.Text,
                                      IsDeleted = u.IsDeleted,
                                      SenderId = u.SenderId,
                                      ReceiverId = u.ReceiverId,
                                      UserName = u.UserName
                                  }).ToListAsync();

            return messages;
        }
        //OK
        public async Task<MessageDTO> GetMessageAsync(int id)
        {
            var message = await (from u in _context.Messages
                                 join c in _context.Users on u.SenderId equals c.Id into grp1
                                 join c in _context.Users on u.ReceiverId equals c.Id into grp2
                                 from g in grp1.DefaultIfEmpty()
                                 from h in grp2.DefaultIfEmpty()
                                 where u.IsDeleted == false
                                 select new MessageDTO
                                 {
                                     Id = u.Id,
                                     SendOn = u.SendOn,
                                     Text = u.Text,
                                     IsDeleted = u.IsDeleted,
                                     SenderId = u.SenderId,
                                     Receiver = u.Receiver,
                                     UserName = u.UserName,
                                     isSeen = u.isSeen,
                                     Sender = u.Sender,
                                     ReceiverId = u.ReceiverId
                                 }).FirstOrDefaultAsync();

            return message;
        }
    }
}
