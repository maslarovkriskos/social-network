﻿using System;
using System.Linq;
using SocialNetwork.Models;
using System.Collections.Generic;
using SocialNetwork.Services.DTOs;

namespace SocialNetwork.Services.Mappers
{
    public static class UserDTOMapper
    {
        public static UserDTO GetDTO(this User item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new UserDTO
            {
                
                Id = item.Id,
                CountryId = item.CountryId,
                Country = item.Country,
                UserName = item.UserName,
                ProfilePhoto = item.ProfilePhoto,
                NormalizedUserName = item.NormalizedUserName,
                NormalizedEmail = item.NormalizedEmail,
                FirstName = item.FirstName,
                LastName = item.LastName,
                DateOfBirth = item.DateOfBirth,
                MonthOfBirth = item.MonthOfBirth,
                YearOfBirth = item.YearOfBirth,
                PasswordHash = item.PasswordHash,
                CurrentLoggedUser = item.CurrentLoggedUser,
                Gender = item.Gender,
                CurrentLoggedUserId = item.CurrentLoggedUserId,
                HistoryLog = item.HistoryLog,
                NewCommentContent = item.NewCommentContent,
                City = item.City,
                Email = item.Email,
                Education = item.Education,
                JobTitle = item.JobTitle,
                Visibility = item.Visibility,
                RelationshipStatus = item.RelationshipStatus,
                Biography = item.Biography,
                RequestersUserFriends = item.RequestersUserFriends,
                AdresseesUserFriends = item.AdresseesUserFriends,
                Photos = item.Photos,
                Posts = item.Posts,
                IsAdmin = item.IsAdmin,
                IsDeleted = item.IsDeleted,
                LoggedUserProfilePicture = item.LoggedUserProfilePicture,
                CoverPhoto = item.CoverPhoto,
                Comments = item.Comments,
                Friends = item.Friends,
                Messages = item.Messages,
                Friends1 = item.Friends1,
                Friends2 = item.Friends2,
                NewsFeed = item.NewsFeed,
            };
        }

        public static User GetUser(this UserDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new User
            {
                Id = item.Id,
                UserName = item.UserName,
                CountryId = item.CountryId,
                Country = item.Country,
                ProfilePhoto = item.ProfilePhoto,
                NormalizedUserName = item.NormalizedUserName,
                NormalizedEmail = item.NormalizedEmail,
                FirstName = item.FirstName,
                LastName = item.LastName,
                DateOfBirth = item.DateOfBirth,
                CoverPhoto = item.CoverPhoto,
                CurrentLoggedUser = item.CurrentLoggedUser,
                MonthOfBirth = item.MonthOfBirth,
                YearOfBirth = item.YearOfBirth,
                PasswordHash = item.PasswordHash,
                NewCommentContent = item.NewCommentContent,
                LoggedUserProfilePicture = item.LoggedUserProfilePicture,
                CurrentLoggedUserId = item.CurrentLoggedUserId,
                Gender = item.Gender,
                HistoryLog = item.HistoryLog,
                City = item.City,
                Email = item.Email,
                Education = item.Education,
                JobTitle = item.JobTitle,
                Visibility = item.Visibility,
                RelationshipStatus = item.RelationshipStatus,
                Biography = item.Biography,
                RequestersUserFriends = item.RequestersUserFriends,
                AdresseesUserFriends = item.AdresseesUserFriends,
                Photos = item.Photos,
                Posts = item.Posts,
                IsAdmin = item.IsAdmin,
                IsDeleted = item.IsDeleted,
                Comments = item.Comments,
                Friends = item.Friends,
                Messages = item.Messages,
                Friends1 = item.Friends1,
                Friends2 = item.Friends2,
                NewsFeed = item.NewsFeed
            };
        }
        public static ICollection<UserDTO> GetDTO(this ICollection<User> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
