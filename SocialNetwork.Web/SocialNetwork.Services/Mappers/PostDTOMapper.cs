﻿using SocialNetwork.Models;
using SocialNetwork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SocialNetwork.Services.Mappers
{
    public static class PostDTOMapper
    {
        public static PostDTO GetDTO(this Post item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new PostDTO
            {
                Comments = item.Comments,
                Content = item.Content,
                Id = item.Id,
                LoggedUser = item.LoggedUser,
                PostPhoto = item.PostPhoto,
                IsFlagged = item.IsFlagged,
                Flags = item.Flags,
                PhotoId = item.PhotoId,
                Photo = item.Photo,
                IsLiked = item.IsLiked,
                CreatedOn = item.CreatedOn,
                Likes = item.Likes,
                UserId = item.UserId,
                User = item.User
            };
        }

        public static Post GetPost(this PostDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new Post
            {
                Comments = item.Comments,
                LoggedUser = item.LoggedUser,
                Flags = item.Flags,
                Content = item.Content,
                PhotoId = item.PhotoId,
                Photo = item.Photo,
                Id = item.Id,
                PostPhoto = item.PostPhoto,
                IsFlagged = item.IsFlagged,
                IsLiked = item.IsLiked,
                CreatedOn = item.CreatedOn,
                Likes = item.Likes,
                UserId = item.UserId,
                User = item.User
            };
        }
        public static ICollection<PostDTO> GetDTO(this ICollection<Post> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
