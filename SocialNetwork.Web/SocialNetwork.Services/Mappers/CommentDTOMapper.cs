﻿using System;
using System.Linq;
using SocialNetwork.Models;
using System.Collections.Generic;
using SocialNetwork.Services.DTOs;

namespace SocialNetwork.Services.Mappers
{
    public static class CommentDTOMapper
    {
        public static CommentDTO GetDTO(this Comment item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new CommentDTO
            {
                Id = item.Id,
                Content = item.Content,
                IsFlagged = item.IsFlagged,
                IsLiked = item.IsLiked,
                CreatedOn = item.CreatedOn,
                Likes = item.Likes,
                Flags = item.Flags,
                PostId = item.PostId,
                Post = item.Post,
                Photo = item.Photo,
                IsDeleted = item.IsDeleted,
                PhotoId = item.PhotoId,
                UserId = item.UserId,
                User = item.User,
            };
        }

        public static Comment GetComment(this CommentDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new Comment
            {
                Id = item.Id,
                Content = item.Content,
                IsFlagged = item.IsFlagged,
                IsLiked = item.IsLiked,
                CreatedOn = item.CreatedOn,
                Likes = item.Likes,
                Flags = item.Flags,
                PostId = item.PostId,
                Post = item.Post,
                Photo = item.Photo,
                IsDeleted = item.IsDeleted,
                PhotoId = item.PhotoId,
                UserId = item.UserId,
                User = item.User,
            };
        }
        public static ICollection<CommentDTO> GetDTO(this ICollection<Comment> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}