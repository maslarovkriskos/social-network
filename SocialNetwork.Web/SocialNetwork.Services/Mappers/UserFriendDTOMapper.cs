﻿using System;
using System.Linq;
using SocialNetwork.Models;
using System.Collections.Generic;
using SocialNetwork.Services.DTOs;

namespace SocialNetwork.Services.Mappers
{
    public static class UserFriendDTOMapper
    {
        public static UserFriendDTO GetDTO(this UserFriend item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new UserFriendDTO
            {
                Adressee = item.Adressee,
                AdresseeId = item.AdresseeId,
                Requester = item.Requester,
                RequesterId = item.RequesterId,
                IsAccepted = item.IsAccepted,
                IsRemoved = item.IsRemoved
            };
        }

        public static UserFriend GetUserFriend(this UserFriendDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new UserFriend
            {
                Adressee = item.Adressee,
                AdresseeId = item.AdresseeId,
                Requester = item.Requester,
                RequesterId = item.RequesterId,
                IsAccepted = item.IsAccepted,
                IsRemoved = item.IsRemoved
            };
        }
        public static ICollection<UserFriendDTO> GetDTO(this ICollection<UserFriend> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
