﻿using SocialNetwork.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SocialNetwork.Database.Configuration;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace SocialNetwork.Database
{
    public class SocialNetworkDbContext : IdentityDbContext<User, Role, int>
    {
        public SocialNetworkDbContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<UserFriend> UserFriends { get; set; }
        public DbSet<HistoryLog> HistoryLogs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CommentConfig());
            modelBuilder.ApplyConfiguration(new PostConfig());
            modelBuilder.ApplyConfiguration(new UserConfig());
            modelBuilder.ApplyConfiguration(new PhotoConfig());
            modelBuilder.ApplyConfiguration(new MessageConfig());

            modelBuilder.Entity<UserFriend>()
                   .HasOne(m => m.Requester)
                   .WithMany(t => t.AdresseesUserFriends)
                   .HasForeignKey(m => m.RequesterId)
                   .OnDelete(DeleteBehavior.Cascade)
                   .IsRequired();

            modelBuilder.Entity<UserFriend>()
                        .HasOne(m => m.Adressee)
                        .WithMany(t => t.RequestersUserFriends)
                        .HasForeignKey(m => m.AdresseeId)
                        .OnDelete(DeleteBehavior.NoAction)
                        .IsRequired();

            modelBuilder.Entity<Message>()
                       .HasOne(m => m.Sender)
                       .WithMany(u => u.Messages)
                       .HasForeignKey(m => m.SenderId)
                       .OnDelete(DeleteBehavior.NoAction)
                       .IsRequired();

            modelBuilder.Entity<Comment>()
                   .HasOne(m => m.Post)
                   .WithMany(u => u.Comments)
                   .HasForeignKey(m => m.PostId)
                   .OnDelete(DeleteBehavior.NoAction)
                   .IsRequired();

            modelBuilder.Entity<Comment>()
                   .HasOne(m => m.User)
                   .WithMany(u => u.Comments)
                   .HasForeignKey(m => m.UserId)
                   .OnDelete(DeleteBehavior.NoAction)
                   .IsRequired();

            modelBuilder.Entity<HistoryLog>()
                   .HasOne(m => m.User)
                   .WithMany(u => u.HistoryLog)
                   .HasForeignKey(m => m.UserId)
                   .OnDelete(DeleteBehavior.NoAction)
                   .IsRequired();


            modelBuilder.Entity<Role>().HasData(
                new Role
                {
                    Id = 1,
                    Name = "member",
                    NormalizedName = "MEMBER",
                },
                new Role
                {
                    Id = 2,
                    Name = "admin",
                    NormalizedName = "ADMIN",
                });

            var hasher = new PasswordHasher<User>();

            User admin = new User
            {
                Id = 1,
                UserName = "admin@friendsfinder.com",
                NormalizedUserName = "ADMIN@FRIENDSFINDER.COM",
                Email = "admin@friendsfinder.com",
                NormalizedEmail = "ADMIN@FRIENDSFINDER.COM",
                SecurityStamp = "7I5VHIJTSZNOT3KDWKNFUV5PVYBHGXN",
            };

            admin.PasswordHash = hasher.HashPassword(admin, "Admin123!");

            modelBuilder.Entity<User>().HasData(admin);

            base.OnModelCreating(modelBuilder);
        }
    }
}
