﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SocialNetwork.Database.Configuration
{
    public class PhotoConfig : IEntityTypeConfiguration<Photo>
    {
        public void Configure(EntityTypeBuilder<Photo> builder)
        {
            builder.HasOne(p => p.User)
                 .WithMany(u => u.Photos)
                 .HasForeignKey(p => p.UserId)
                 .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(p => p.Post)
                 .WithMany(u => u.Photos)
                 .HasForeignKey(p => p.PostId)
                 .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
