# Social Network

A Social Network Implementaion by Kristiyan Maslarov and Nik Nikolov.

API Controllers:
CommentAPIController:
1.	GETALLCOMMENT: https://localhost:5001/api/comment
[
    {
        "id": 9,
        "content": "New Kris",
        "isFlagged": false,
        "likes": 0,
        "flags": 0,
        "isLiked": false,
        "createdOn": "2020-11-07T00:00:00",
        "postId": 2,
        "post": null
    },
    {
        "id": 10,
        "content": "C# Tigers",
        "isFlagged": true,
        "likes": 0,
        "flags": 0,
        "isLiked": true,
        "createdOn": "2020-11-06T00:00:00",
        "postId": 2,
        "post": null
    },
    {
        "id": 11,
        "content": "C# Tigers",
        "isFlagged": true,
        "likes": 0,
        "flags": 0,
        "isLiked": true,
        "createdOn": "2020-11-06T00:00:00",
        "postId": 2,
        "post": null
    }
]
2.	GETCOMMENT: https://localhost:5001/api/comment/5
{
    "id": 5,
    "content": "JS Tigers",
    "isFlagged": false,
    "likes": 0,
    "flags": 0,
    "isLiked": false,
    "createdOn": "2020-11-06T00:00:00",
    "postId": 2,
    "post": null
}
3.	DELETECOMMENT: https://localhost:5001/api/comment/5
{
    //IsDeleted = true; 

    "id": 5,
    "content": "JS Tigers",
    "isFlagged": false,
    "likes": 1,
    "flags": 1,
    "isLiked": false,
    "createdOn": "2020-11-06T00:00:00",
    "postId": 2,
    "post": null
}
4.	UPDATECOMMENT: https://localhost:5001/api/comment/?id=5&newcontent=dada
{
    "id": 5,
    "content": "JS Tigers",
    "isFlagged": false,
    "likes": 1,
    "flags": 1,
    "isLiked": false,
    "createdOn": "2020-11-06T00:00:00",
    "postId": 2,
    "post": null
}
5.	POSTCOMMENT: https://localhost:5001/api/comment
{
    "id": 11,
    "content": "C# Tigers",
    "isFlagged": true,
    "likes": 3,
    "flags": 2,
    "isLiked": true,
    "createdOn": "2020-11-06T00:00:00",
    "postId": 2,
    "post": null
}

CountryAPIControllers:
1.  GETALLCOUNTRIES: https://localhost:5001/api/country
{
        "id": 2,
        "name": "Germany",
        "users": []        
}
2.  GETCOUNTRY: https://localhost:5001/api/country/2
{
    "id": 2,
    "name": "Germany",
    "users": []
}
3.  DELETECOUNTRY: https://localhost:5001/api/country/1
{
    //IsDeleted = true; 
    id: 1,
    name: "Germany",
    users: []
}
4.  UPDATECOUNTRY: https://localhost:5001/api/country/?id=2&newname=Germany
{
    "id": 2,
    "name": "Germany",
    "users": null
}
5.	POSTCOUNTRY: https://localhost:5001/api/coUNTRY
{
    "success": true,
    "message": "New country has been created successfully!"
}
{
        "id": 3,
        "name": "England",
        "users": []
}
PhotoAPIController:
1. UPLOADPHOTO: https://localhost:5001/api/photo
{
    "id": 2,
    "postId": 2,
    "post": null,
    "userId": 2,
    "user": null
}
2. DELETEPHOTO: https://localhost:5001/api/photo/1
{
    //IsDeleted = true;
    "id": 1,
    "postId": 2,
    "post": null,
    "userId": 2,
    "user": null
}
PostAPIController: 
1. GETALLPOSTS: https://localhost:5001/api/post
{ 
        "id": 2,
        "createdOn": "2020-11-06T00:00:00",
        "isLiked": false,
        "likes": 0,
        "content": "ebahmajkatmi",
        "isFlagged": false,
        "flags": 0,
        "userId": 2,
        "user": {
            "firstName": null,
            "lastName": null,
            "age": null,
            "emailConfirmed": true,
            "lockoutEnabled": true,
            "countryId": null,
            "country": null,
            "education": null,
            "isDeleted": false,
            "jobTitle": null,
            "adress": null,
            "biography": null,
            "isAdmin": false,
            "visibility": 0,
            "photos": null,
            "relationshipStatus": 0,
            "requestersUserFriends": null,
            "adresseesUserFriends": null,
            "posts": null,
            "messages": null,
            "id": 2,
            "userName": "nnickolov1@gmail.com",
            "normalizedUserName": "NNICKOLOV1@GMAIL.COM",
            "email": "nnickolov1@gmail.com",
            "normalizedEmail": "NNICKOLOV1@GMAIL.COM",
            "passwordHash": "AQAAAAEAACcQAAAAEBjz7byihpMi/09hwjolRlg4KrmeB/1/h/83p0F5W1TqRgbLjPbjf4HqXW6LkbdyEg==",
            "securityStamp": "HPEQRWUW2GM7J7FASJ7WEBLNE2LY22EX",
            "concurrencyStamp": "76a318d4-f836-4412-8026-78cef621e420",
            "phoneNumber": null,
            "phoneNumberConfirmed": false,
            "twoFactorEnabled": false,
            "lockoutEnd": null,
            "accessFailedCount": 0
        },
        "comments": []
    },
    {
        "id": 3,
        "createdOn": "0001-01-01T00:00:00",
        "isLiked": false,
        "likes": 0,
        "content": null,
        "isFlagged": false,
        "flags": 0,
        "userId": 2,
        "user": {
            "firstName": null,
            "lastName": null,
            "age": null,
            "emailConfirmed": true,
            "lockoutEnabled": true,
            "countryId": null,
            "country": null,
            "education": null,
            "isDeleted": false,
            "jobTitle": null,
            "adress": null,
            "biography": null,
            "isAdmin": false,
            "visibility": 0,
            "photos": null,
            "relationshipStatus": 0,
            "requestersUserFriends": null,
            "adresseesUserFriends": null,
            "posts": null,
            "messages": null,
            "id": 2,
            "userName": "nnickolov1@gmail.com",
            "normalizedUserName": "NNICKOLOV1@GMAIL.COM",
            "email": "nnickolov1@gmail.com",
            "normalizedEmail": "NNICKOLOV1@GMAIL.COM",
            "passwordHash": "AQAAAAEAACcQAAAAEBjz7byihpMi/09hwjolRlg4KrmeB/1/h/83p0F5W1TqRgbLjPbjf4HqXW6LkbdyEg==",
            "securityStamp": "HPEQRWUW2GM7J7FASJ7WEBLNE2LY22EX",
            "concurrencyStamp": "76a318d4-f836-4412-8026-78cef621e420",
            "phoneNumber": null,
            "phoneNumberConfirmed": false,
            "twoFactorEnabled": false,
            "lockoutEnd": null,
            "accessFailedCount": 0
        },
        "comments": []
    }
 2. GETPOST - https://localhost:5001/api/post/2
    {
    "id": 2,
    "createdOn": "2020-11-06T00:00:00",
    "isLiked": false,
    "likes": 0,
    "content": "ebahmajkatmi",
    "isFlagged": false,
    "flags": 0,
    "userId": 2,
    "user": {
        "firstName": null,
        "lastName": null,
        "age": null,
        "emailConfirmed": true,
        "lockoutEnabled": true,
        "countryId": null,
        "country": null,
        "education": null,
        "isDeleted": false,
        "jobTitle": null,
        "adress": null,
        "biography": null,
        "isAdmin": false,
        "visibility": 0,
        "photos": null,
        "relationshipStatus": 0,
        "requestersUserFriends": null,
        "adresseesUserFriends": null,
        "posts": null,
        "messages": null,
        "id": 2,
        "userName": "nnickolov1@gmail.com",
        "normalizedUserName": "NNICKOLOV1@GMAIL.COM",
        "email": "nnickolov1@gmail.com",
        "normalizedEmail": "NNICKOLOV1@GMAIL.COM",
        "passwordHash": "AQAAAAEAACcQAAAAEBjz7byihpMi/09hwjolRlg4KrmeB/1/h/83p0F5W1TqRgbLjPbjf4HqXW6LkbdyEg==",
        "securityStamp": "HPEQRWUW2GM7J7FASJ7WEBLNE2LY22EX",
        "concurrencyStamp": "76a318d4-f836-4412-8026-78cef621e420",
        "phoneNumber": null,
        "phoneNumberConfirmed": false,
        "twoFactorEnabled": false,
        "lockoutEnd": null,
        "accessFailedCount": 0
    },
    "comments": []
}
3. CREATEPOST - https://localhost:5001/api/post
{
    "id": 4,
    "createdOn": "0001-01-01T00:00:00",
    "isLiked": false,
    "likes": 0,
    "content": null,
    "isFlagged": false,
    "flags": 0,
    "userId": 2,
    "user": null,
    "comments": []
}
4. DELETEPOST - https://localhost:5001/api/post/2
{
    "message": "This post has been deleted successfully!",
    "success": true
}
5. UPDATEPOST - https://localhost:5001/api/post/?id=2&newcontent=newContent
{
    "id": 2,
    "createdOn": "2020-11-06T00:00:00",
    "isLiked": false,
    "likes": 0,
    "content": "newContent",
    "isFlagged": false,
    "flags": 0,
    "userId": 2,
    "user": null,
    "comments": []
}
6. FLAGPOST - https://localhost:5001/api/post/flag/id=2
{
    "id": 2,
    "createdOn": "2020-11-06T00:00:00",
    "isLiked": true,
    "likes": 3,
    "content": "newContent",
    "isFlagged": true,
    "flags": 4,
    "userId": 2,
    "user": null,
    "comments": []
}
7. LIKEPOST - https://localhost:5001/api/post/like/id=2
{
    "id": 2,
    "createdOn": "2020-11-06T00:00:00",
    "isLiked": true,
    "likes": 3,
    "content": "newContent",
    "isFlagged": true,
    "flags": 4,
    "userId": 2,
    "user": null,
    "comments": []
}
UserAPIController:
1. GETALLUSERS - https://localhost:5001/api/user
[
    {
        "id": 1,
        "firstName": null,
        "lastName": null,
        "age": null,
        "countryId": null,
        "country": null,
        "education": null,
        "jobTitle": null,
        "adress": null,
        "email": "admin@admin.admin",
        "biography": null,
        "photoId": 0,
        "profilePhoto": null,
        "photos": [],
        "relationshipStatus": 0,
        "requestersUserFriends": null,
        "adresseesUserFriends": null,
        "newsFeed": [],
        "posts": []
    },
    {
        "id": 2,
        "firstName": null,
        "lastName": null,
        "age": null,
        "countryId": null,
        "country": null,
        "education": null,
        "jobTitle": null,
        "adress": null,
        "email": "nnickolov1@gmail.com",
        "biography": null,
        "photoId": 0,
        "profilePhoto": null,
        "photos": [],
        "relationshipStatus": 0,
        "requestersUserFriends": null,
        "adresseesUserFriends": null,
        "newsFeed": [],
        "posts": []
    }
]
2. GET USER - https://localhost:5001/api/user/2
{
    "id": 2,
    "firstName": null,
    "lastName": null,
    "age": null,
    "countryId": null,
    "country": null,
    "education": null,
    "jobTitle": null,
    "adress": null,
    "email": "nnickolov1@gmail.com",
    "biography": null,
    "photoId": 0,
    "profilePhoto": null,
    "photos": [],
    "relationshipStatus": 0,
    "requestersUserFriends": null,
    "adresseesUserFriends": null,
    "newsFeed": [],
    "posts": []
}
3. CREATE USER - https://localhost:5001/api/user
{
    "success": true,
    "message": "You have been registered!"
}
4. GET USER BY NAME - https://localhost:5001/api/user/userbyname?firstName=Kris&lastName=Maslarov
{
    "id": 3,
    "firstName": "Kris",
    "lastName": "Maslarov",
    "age": null,
    "countryId": null,
    "country": null,
    "education": null,
    "jobTitle": null,
    "adress": null,
    "email": "maslarovkriskos@gmail.com",
    "biography": null,
    "photoId": 0,
    "profilePhoto": null,
    "photos": [],
    "relationshipStatus": 0,
    "requestersUserFriends": null,
    "adresseesUserFriends": null,
    "newsFeed": [],
    "posts": []
}
5. UPDATE USER - https://localhost:5001/api/user?id=3
{
    "firstName": "Murjooo",
    "lastName": "Murjeeev",
    "age": 18,
    "countryId": null,
    "country": null,
    "education": null,
    "jobTitle": null,
    "adress": null,
    "email": "maslarovkriskos@gmail.com",
    "biography": null,
    "photoId": 0,
    "profilePhoto": null,
    "photos": [],
    "relationshipStatus": 2,
    "requestersUserFriends": null,
    "adresseesUserFriends": null,
    "newsFeed": [],
    "posts": []
}